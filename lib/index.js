import parsePlugin from './parse';

const plugins = {
  tika_parse: parsePlugin,
};

export { plugins };
export default { plugins };
